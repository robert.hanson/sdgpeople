import type { StoryObj } from '@storybook/vue3';

type Story<T> = StoryObj<T>;
const withViewport = <T>(viewport: string, story: Story<T>): Story<T> => ({
  ...story,
  globals: {
    // 👇 Override default viewport for this story
    viewport: { value: viewport, isRotated: false }
  }
});

export default withViewport;

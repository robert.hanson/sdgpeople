import type { Meta, StoryObj } from '@storybook/vue3';
import EffectsBubbles from './EffectsBubbles.vue';
import '../../assets/main.css';

// More on how to set up stories at: https://storybook.js.org/docs/writing-stories
const meta = {
  component: EffectsBubbles,
  // This component will have an automatically generated docsPage entry: https://storybook.js.org/docs/writing-docs/autodocs
  tags: ['autodocs'],
  args: {},
  render: (args: any) => ({
    components: { EffectsBubbles },
    setup() {
      return { args };
    },
    template: `
    <div>
      &nbsp;
      <EffectsBubbles v-bind="args">+1</EffectsBubbles>
    </div>`
  })
} satisfies Meta<typeof EffectsBubbles>;

export default meta;
type Story = StoryObj<typeof meta>;
/*
 *👇 Render functions are a framework specific feature to allow you control on how the component renders.
 * See https://storybook.js.org/docs/api/csf
 * to learn how to use render functions.
 */
export const Playground: Story = {
  args: {}
};

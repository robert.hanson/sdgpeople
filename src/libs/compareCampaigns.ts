import type { Person } from '@/types/Person';
import isEqual from 'lodash/isEqual';

type CampaignDifferences = { added: Person[]; removed: Person[]; updated: { previous: Person; current: Person }[]; unchanged: Person[] };

const compareCampaigns = (previousCampaignData: Person[], currentCampaignData: Person[]) => {
  const users = new Set<string>();
  previousCampaignData.forEach((person) => users.add(person.displayName));
  currentCampaignData.forEach((person) => users.add(person.displayName));

  const names = Array.from(users).sort();
  const updated = names.reduce<CampaignDifferences>(
    (acc, name) => {
      const previous = previousCampaignData.find((person) => person.displayName === name);
      const current = currentCampaignData.find((person) => person.displayName === name);

      if (previous && current) {
        if (isEqual(previous, current)) {
          acc.unchanged.push(current);
        } else {
          acc.updated.push({ previous, current });
        }
      } else if (previous) {
        acc.removed.push(previous);
      } else {
        acc.added.push(current as Person);
      }

      return acc;
    },
    { added: [], removed: [], updated: [], unchanged: [] }
  );

  return updated;
};

type UpdatedPerson = { previous: Person; current: Person };
type UpdatedPeople = UpdatedPerson[];
type Update = { displayName: string; key: string; previous: string | string[]; current: string | string[] };

const compareUpdatedPeople = (updatedPeople: UpdatedPeople): Update[] => {
  const diffs = updatedPeople.reduce<Update[]>((acc, updatedPerson) => {
    const { previous, current } = updatedPerson;
    Object.keys(current).forEach((key) => {
      if (!isEqual(previous[key as keyof Person], current[key as keyof Person])) {
        acc.push({ displayName: current.displayName, key, previous: previous[key as keyof Person], current: current[key as keyof Person] });
      }
    });
    return acc;
  }, []);
  return diffs;
};

export { type Update, compareCampaigns, compareUpdatedPeople };

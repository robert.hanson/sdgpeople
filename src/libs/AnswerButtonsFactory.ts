import type { Answers } from '@/types/Answers';

const answerButtonsFactory = (answers: string[], correctAnswer: string): Answers => {
  const answerButtons = answers.map((answer) => ({
    buttonText: answer,
    isCorrectAnswer: false,
    hasBeenPicked: false
  }));
  const correctButton = answerButtons.find((answer) => answer.buttonText === correctAnswer);
  if (correctButton) {
    correctButton.isCorrectAnswer = true;
  }
  return {
    pickedCorrectAnswer: false,
    pickedIncorrectAnswer: false,
    answers: answerButtons
  };
};

export default answerButtonsFactory;

import { defineStore } from 'pinia';
import { useGameStore } from './gameStore';
import type { Person } from '@/types/Person';
import { ref } from 'vue';
import { useFirebaseStore } from './firebaseStore';
import hashBlobUrl from '@/libs/hashBlobUrl';
type Campaign = {
  id: string;
  name: string;
  date: string;
  snapshot?: Person[];
};

export const useUpdateDataCampaignStore = defineStore('updateDataCampaignStore', () => {
  const firebaseStore = useFirebaseStore();
  const gameStore = useGameStore();
  const campaigns = ref<Campaign[]>([]);

  async function getCampaigns() {
    return firebaseStore.getCampaigns();
  }

  async function getCurrentCampaignData(): Promise<Person[]> {
    const usersCopy = gameStore.users.map((user) => ({ ...user }));
    for (const user of usersCopy) {
      if (user.image) {
        user.image = await hashBlobUrl(user.image);
      } else {
        user.image = 'No Image';
      }
    }
    return usersCopy;
  }

  async function addCampaign(name: string) {
    const usersCopy = await getCurrentCampaignData();
    const dt = new Date().toDateString();
    const newCampaign = { id: name, name, date: dt, snapshot: usersCopy };
    await firebaseStore.addCampaign(newCampaign);
    campaigns.value.push(newCampaign);
    return newCampaign;
  }

  async function removeCampaign(name: string) {
    await firebaseStore.removeCampaign(name);
  }

  return { getCurrentCampaignData, getCampaigns, addCampaign, removeCampaign };
});

export type { Campaign };

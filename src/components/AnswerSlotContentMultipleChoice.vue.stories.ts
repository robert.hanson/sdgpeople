import type { Meta, StoryObj } from '@storybook/vue3';
import { action } from '@storybook/addon-actions';
import '../assets/main.css';
import AnswerSlotContentMultipleChoice from './AnswerSlotContentMultipleChoice.vue';
import defaultAnswers from './defaultAnswers';
const meta = {
  component: AnswerSlotContentMultipleChoice,
  tags: ['autodocs'],
  args: {
    answers: defaultAnswers(),
    completed: false
  },
  render: (args: any) => ({
    components: { AnswerSlotContentMultipleChoice },
    setup() {
      return { args };
    },
    methods: {
      onAnswerClick: action('onAnswerClick'),
      onNext: action('onNext')
    },
    template: `<AnswerSlotContentMultipleChoice v-bind="args" @onAnswerClick="onAnswerClick" @onNext="onNext" />`
  })
} satisfies Meta<typeof AnswerSlotContentMultipleChoice>;

export default meta;
type Story = StoryObj<typeof meta>;
const answers = defaultAnswers();
answers.answers[0].isCorrectAnswer = true;
answers.answers[0].hasBeenPicked = true;
answers.answers[1].hasBeenPicked = true;
export const Playground: Story = {
  args: {
    answers: defaultAnswers(),
    completed: false
  }
};

export const Completed: Story = {
  args: {
    answers,
    completed: true
  }
};

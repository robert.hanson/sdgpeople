import type { Meta, StoryObj } from '@storybook/vue3';
import SdgRotationOne from './SdgRotationOne.vue';
import '../../assets/main.css';

// More on how to set up stories at: https://storybook.js.org/docs/writing-stories
const meta = {
  component: SdgRotationOne,
  // This component will have an automatically generated docsPage entry: https://storybook.js.org/docs/writing-docs/autodocs
  tags: ['autodocs'],
  args: {},
  render: (args: any) => ({
    components: { SdgRotationOne },
    setup() {
      return { args };
    },
    template: `
    <div>
      &nbsp;
      <SdgRotationOne v-bind="args">+1</SdgRotationOne>
    </div>`
  })
} satisfies Meta<typeof SdgRotationOne>;

export default meta;
type Story = StoryObj<typeof meta>;
/*
 *👇 Render functions are a framework specific feature to allow you control on how the component renders.
 * See https://storybook.js.org/docs/api/csf
 * to learn how to use render functions.
 */
export const Playground: Story = {
  args: {}
};

import DateFormat from '@/libs/DateFormat';
import moment from 'moment';
import { type IDateCategory, Year } from './IDateCategory';

export default class DateCategory implements IDateCategory {
  public days: number;

  public year: number;

  public hireYear: number;

  public employeeHireDate: string;

  public classificationString: string;

  public static Categorize(isoDateString: string, today: moment.Moment = moment()) {
    return new DateCategory(isoDateString, moment(today));
  }

  public constructor(isoDateString: string, today: moment.Moment = moment()) {
    if (isoDateString === undefined || isoDateString.startsWith('0001-01-01')) {
      this.employeeHireDate = '';
      this.hireYear = 0;
      this.days = -1;
      this.year = Year.Unknown;
    } else {
      const hiredOn = moment(isoDateString);

      this.employeeHireDate = DateFormat.format(hiredOn);
      this.hireYear = parseInt(hiredOn.format('YYYY'));
      this.days = this.daysBetween(today, hiredOn);
      if (this.days <= 90) {
        this.year = Year.LessThan3Months;
      } else if (this.days <= 180) {
        this.year = Year.LessThan6Months;
      } else if (this.days < 365) {
        this.year = Year.LessThan1Year;
      } else {
        this.year = this.yearsBetween(today, hiredOn);
      }
    }

    this.classificationString = DateCategory.setClassificationString(this.year);
  }

  public static setClassificationString(year: number): string {
    switch (year) {
      case Year.Unknown:
        return 'Not Specified';
      case Year.LessThan3Months:
        return 'Less than 3 months';
      case Year.LessThan6Months:
        return 'Less than 6 months';
      case Year.LessThan1Year:
        return 'Less than 1 year';
      default:
        return `${year} year${year === 1 ? '' : 's'}`;
    }
  }

  private daysBetween(first: moment.Moment, second: moment.Moment): number {
    return first.diff(second, 'days');
  }

  private yearsBetween(first: moment.Moment, second: moment.Moment): number {
    return first.diff(second, 'years');
  }
}

import type { Answer } from './Answer';

interface Answers {
  pickedCorrectAnswer: boolean;
  pickedIncorrectAnswer: boolean;
  answers: Answer[];
}
export { type Answers };

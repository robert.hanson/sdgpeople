import { type Preview, setup } from '@storybook/vue3';
import { MINIMAL_VIEWPORTS } from '@storybook/addon-viewport';
import seedrandom from 'seedrandom';
import { createPinia } from 'pinia';
import { App } from 'vue';
import { Vue3Mq } from 'vue3-mq';

const pinia = createPinia();
setup((app: App) => {
  app.use(pinia);
  app.use(Vue3Mq, {
    preset: 'tailwind'
  });
});

export const decorators = [
  (story) => {
    // Code to run before each story
    seedrandom('storybook', { global: true });
    return story();
  }
];

const customViewports = [360, 568, 666, 768, 1024, 1300].reduce((accum, width) => {
  accum[`viewport${width}`] = {
    name: `Width ${width}`,
    styles: {
      width: `${width}px`,
      height: '800px'
    }
  };
  return accum;
}, {});

const preview: Preview = {
  parameters: {
    viewport: {
      options: customViewports
    },
    controls: {
      matchers: {
        color: /(background|color)$/i,
        date: /Date$/i
      }
    }
  },
  initialGlobals: {
    //viewport: { value: 'ipad', isRotated: false }
  }
};

export default preview;

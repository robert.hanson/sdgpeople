import type { Meta, StoryObj } from '@storybook/vue3';
import '../assets/main.css';
import AboutPerson from './AboutPerson.vue';
import person from '../stories/fakeData/person.json';

const meta = {
  component: AboutPerson,
  tags: ['autodocs'],
  args: {
    person
  },
  render: (args: any) => ({
    components: { AboutPerson },
    setup() {
      return { args };
    },
    template: '<AboutPerson v-bind="args" />'
  })
} satisfies Meta<typeof AboutPerson>;

export default meta;
type Story = StoryObj<typeof meta>;
/*
 *👇 Render functions are a framework specific feature to allow you control on how the component renders.
 * See https://storybook.js.org/docs/api/csf
 * to learn how to use render functions.
 */
export const Playground: Story = {};

export const PlaygroundXS: Story = { globals: { viewport: { value: 'viewport360', isRotated: false } }, tags: ['viewport360'] };
export const PlaygroundS: Story = { globals: { viewport: { value: 'viewport568', isRotated: false } }, tags: ['viewport568'] };
export const PlaygroundTablet: Story = { globals: { viewport: { value: 'viewport768', isRotated: false } }, tags: ['viewport768'] };

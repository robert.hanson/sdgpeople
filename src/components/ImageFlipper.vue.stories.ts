import type { Meta, StoryObj } from '@storybook/vue3';
import '../assets/main.css';
import ImageFlipper from './ImageFlipper.vue';

const meta = {
  component: ImageFlipper,
  tags: ['autodocs'],
  args: {
    imageUrl: 'https://live-production.wcms.abc-cdn.net.au/448394f72db633f69e0f55ef77b7c850?impolicy=wcms_crop_resize&cropH=2160&cropW=3840&xPos=0&yPos=0&width=862&height=485',
    isActive: false
  },
  render: (args: any) => ({
    components: { ImageFlipper },
    setup() {
      return { args };
    },
    template: '<ImageFlipper v-bind="args"></ImageFlipper>'
  })
} satisfies Meta<typeof ImageFlipper>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Playground: Story = {
  args: { ...meta.args }
};

export const CustomBackgroundColor: Story = {
  args: {
    ...meta.args,
    backgroundColor: 'red-500'
  }
};

function randomizedList<T>(list: T[]): T[] {
  return list
    .map((item) => ({ item, order: Math.random() }))
    .sort((a: any, b: any) => a.order - b.order)
    .map((x) => x.item);
}
export default randomizedList;

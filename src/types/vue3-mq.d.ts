declare module 'vue3-mq' {
  import type { DefineComponent } from 'vue';
  const MqResponsive: DefineComponent<{}, {}, any>;
  export { MqResponsive };
  export const Vue3Mq: Plugin<[]>;
}

import type { Meta, StoryObj } from '@storybook/vue3';
import { action } from '@storybook/addon-actions';
import WhoAmI from './WhoAmI.vue';
import people from '../../stories/fakeData/people.json';
import { useGameStore } from '@/stores/gameStore';
import type { Answer } from '@/types/Answer';
import { ref } from 'vue';
import { useWhoAmIStore } from '@/stores/whoAmIStore';
import { vueRouter } from 'storybook-vue3-router';
import type { RouteRecordRaw } from 'vue-router';
const customRoutes = [
  {
    path: '/',
    name: 'aboutperson'
  }
] as RouteRecordRaw[];

// More on how to set up stories at: https://storybook.js.org/docs/writing-stories
const meta = {
  component: WhoAmI,
  // This component will have an automatically generated docsPage entry: https://storybook.js.org/docs/writing-docs/autodocs
  tags: ['autodocs'],
  args: {},
  render: (args: any) => ({
    components: { WhoAmI },
    setup() {
      const store = useGameStore();
      store.users = people.map((person) => ({ ...person, image: '/cooper_miller.jpg', aboutMe: person.aboutMe || 'aboutMe', userType: person.userType || 'userType' }));

      const whoAmIStore = useWhoAmIStore();
      const correctAnswer = ref((whoAmIStore.answers.answers.find((x) => x.isCorrectAnswer) || {}).buttonText);
      store.next = () => {
        action('next')();
        return Promise.resolve();
      };
      store.addOnePoint = () => {
        action('addOnePoint')();
        return Promise.resolve();
      };
      return { ...args, correctAnswer };
    },
    methods: {
      setCorrectAnswer(answer: Answer) {
        this.correctAnswer.value = answer.buttonText;
      }
    },
    template: `
    <div>
      <WhoAmI v-bind="args"/>
      <div>(Correct answer: {{correctAnswer}})</div>
    </div>`
  }),
  decorators: [vueRouter(customRoutes)]
} satisfies Meta<typeof WhoAmI>;

export default meta;
type Story = StoryObj<typeof meta>;
export const Playground: Story = {
  args: {}
};

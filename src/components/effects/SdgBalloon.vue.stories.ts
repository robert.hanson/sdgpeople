import type { Meta, StoryObj } from '@storybook/vue3';

import SdgBalloon from './SdgBalloon.vue';

// More on how to set up stories at: https://storybook.js.org/docs/writing-stories
const meta = {
  component: SdgBalloon,
  // This component will have an automatically generated docsPage entry: https://storybook.js.org/docs/writing-docs/autodocs
  tags: ['autodocs'],
  args: {},
  render: (args: any) => ({
    components: { SdgBalloon },
    setup() {
      return { args };
    },
    template: `
      <div>
        &nbsp;
        <SdgBalloon v-bind="args">+1</SdgBalloon>
    </div>`
  })
} satisfies Meta<typeof SdgBalloon>;

export default meta;
type Story = StoryObj<typeof meta>;
/*
 *👇 Render functions are a framework specific feature to allow you control on how the component renders.
 * See https://storybook.js.org/docs/api/csf
 * to learn how to use render functions.
 */
export const Playground: Story = {
  args: {
    appearance: 0,
    noAnimation: true
  }
};

export const One: Story = {
  args: {
    appearance: 0,
    noAnimation: true
  }
};

export const Two: Story = {
  args: {
    appearance: 1,
    noAnimation: true
  }
};

export const Three: Story = {
  args: {
    appearance: 2,
    noAnimation: true
  }
};
export const Four: Story = {
  args: {
    appearance: 3,
    noAnimation: true
  }
};
export const Five: Story = {
  args: {
    appearance: 4,
    noAnimation: true
  }
};

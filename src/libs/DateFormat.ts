import moment from 'moment';

export default class DateFormat {
  public static format(date: string | moment.Moment) {
    if (!date || date === '0001-01-01T00:00:00Z') return '';
    return moment(date).utc(false).format('MM/DD/YYYY');
  }

  public static birthdate(date: string | moment.Moment) {
    if (!date || date === '0001-01-01T00:00:00Z') return '';
    return moment(date).utc(false).format('MM/DD');
  }

  public static getYear(date: string | moment.Moment) {
    if (!date || date === '0001-01-01T00:00:00Z') return '';
    return moment(date).utc(false).format('YYYY');
  }
}

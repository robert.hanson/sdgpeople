import type { Meta, StoryObj } from '@storybook/vue3';
import SdgFireworks from './SdgFireworks.vue';

// More on how to set up stories at: https://storybook.js.org/docs/writing-stories
const meta = {
  component: SdgFireworks,
  // This component will have an automatically generated docsPage entry: https://storybook.js.org/docs/writing-docs/autodocs
  tags: ['autodocs'],
  args: {},
  render: (args: any) => ({
    components: { SdgFireworks },
    setup() {
      return { args };
    },
    template: `
      <div>
      &nbsp;
      <SdgFireworks v-bind="args"></SdgFireworks>
      </div>`
  })
} satisfies Meta<typeof SdgFireworks>;

export default meta;
type Story = StoryObj<typeof meta>;
/*
 *👇 Render functions are a framework specific feature to allow you control on how the component renders.
 * See https://storybook.js.org/docs/api/csf
 * to learn how to use render functions.
 */
export const Playground: Story = {
  args: {}
};

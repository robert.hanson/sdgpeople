interface CashInRequest {
  name: string;
  sdgBucks: number;
  date: number;
  process?: boolean;
  credited?: boolean;
  creditDate?: number;
}

export type { CashInRequest };

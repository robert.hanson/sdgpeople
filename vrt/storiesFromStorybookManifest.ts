import fs from 'fs';
import path from 'path';
import StoryIdToUrl from './storyIdToUrl';
import { StorybookManifestPath } from './constants';

interface IStory {
  file: string;
  story: string;
  url: string;
  width?: number;
}

type IStorybookManifest = {
  id: string;
  type: string;
  title: string;
  importPath: string;
  name: string;
  tags: string[];
};

function widthFromTags(tags: string[]): number | undefined {
  const widthTag = tags.find((tag) => tag.startsWith('viewport'));
  if (widthTag) {
    return parseInt(widthTag.replace('viewport', ''));
  }
  return undefined;
}

function storiesFromStorybookManifest(): IStory[] {
  const manifestPath = path.resolve(process.cwd(), StorybookManifestPath);
  const storiesManifest = JSON.parse(fs.readFileSync(manifestPath, 'utf8'));
  const stories = (Object.entries(storiesManifest.entries) as [string, IStorybookManifest][])
    .filter(([, value]) => value.type === 'story')
    .filter(([, value]) => /(ts|tsx)$/.test(value.importPath))
    .map(([, value]) => ({
      type: 'story',
      file: value.importPath.replace(/.*stories\//, ''),
      story: `${value.title}--${value.name}`,
      url: StoryIdToUrl(value.id),
      width: widthFromTags(value.tags)
    }));
  return stories;
}

export default storiesFromStorybookManifest;
export type { IStory };

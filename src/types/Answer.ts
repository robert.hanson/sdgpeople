interface Answer {
  buttonText: string;
  isCorrectAnswer: boolean;
  hasBeenPicked: boolean;
}

export { type Answer };

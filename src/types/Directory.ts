export type SortByOptionKeys = 'employeeHireDate' | 'displayName';
export type SortByOptions = { label: string; value: SortByOptionKeys }[];

import type { Meta, StoryObj } from '@storybook/vue3';
import '../assets/main.css';
import ImageFlipperBlock from './ImageFlipperBlock.vue';

const meta = {
  component: ImageFlipperBlock,
  tags: ['autodocs'],
  args: {
    people: ['adam_johnson', 'anh_duong', 'ben_fischer', 'ben_stori', 'gucci_aguh', 'kate_williamson', 'lindsey_hansen', 'nick_bruecken', 'cooper_miller'],
    timers: [4100, 3600, 2250, 4725],
    initialFlippedStates: [false, true, false, false]
  },
  render: (args: any) => ({
    components: { ImageFlipperBlock },
    setup() {
      return { args };
    },
    template: '<ImageFlipperBlock v-bind="args"></ImageFlipperBlock>'
  })
} satisfies Meta<typeof ImageFlipperBlock>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Playground: Story = {
  args: {}
};

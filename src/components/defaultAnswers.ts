const answers = () =>
  ['Cooper Miller', 'Anna Boeder', 'Jackson Oppenheim', 'Mark Reid'].map((text) => ({
    buttonText: text,
    isCorrectAnswer: false,
    hasBeenPicked: false
  }));

const defaultAnswers = () => ({
  pickedCorrectAnswer: false,
  pickedIncorrectAnswer: false,
  answers: answers()
});

export default defaultAnswers;

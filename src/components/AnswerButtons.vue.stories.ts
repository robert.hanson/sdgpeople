import type { Meta, StoryObj } from '@storybook/vue3';
import { action } from '@storybook/addon-actions';
import '../assets/main.css';
import AnswerButtons from './AnswerButtons.vue';
import defaultAnswers from './defaultAnswers';

const meta = {
  component: AnswerButtons,
  tags: ['autodocs'],
  args: {
    answers: defaultAnswers()
  },
  render: (args: any) => ({
    components: { AnswerButtons },
    setup() {
      return { args };
    },
    methods: {
      handleClick: action('button-clicked')
    },
    template: `<AnswerButtons v-bind="args" @onAnswerClick="handleClick" />`
  })
} satisfies Meta<typeof AnswerButtons>;

export default meta;
type Story = StoryObj<typeof meta>;
/*
 *👇 Render functions are a framework specific feature to allow you control on how the component renders.
 * See https://storybook.js.org/docs/api/csf
 * to learn how to use render functions.
 */

let storyAnswers = defaultAnswers();
storyAnswers.answers[0].isCorrectAnswer = true;
storyAnswers.answers[0].hasBeenPicked = true;
export const SingleButtonCorrect: Story = {
  args: {
    answers: { ...storyAnswers, answers: [storyAnswers.answers[0]] }
  }
};

storyAnswers = defaultAnswers();
storyAnswers.answers[0].hasBeenPicked = true;
export const SingleButtonIncorrect: Story = {
  args: {
    answers: { ...storyAnswers, answers: [storyAnswers.answers[0]] }
  }
};

storyAnswers = defaultAnswers();
export const FourButton: Story = {
  args: {
    answers: { ...storyAnswers }
  }
};

storyAnswers = defaultAnswers();
[0, 1, 2, 3].forEach((index) => {
  storyAnswers.answers[index].buttonText = 'x x';
  storyAnswers.answers[index].hasBeenPicked = true;
  storyAnswers.answers[index].isCorrectAnswer = false;
});
export const SelectedIncorrectAnswer: Story = {
  args: {
    answers: { ...storyAnswers }
  }
};

storyAnswers = defaultAnswers();
storyAnswers.answers[0].isCorrectAnswer = true;
storyAnswers.answers[0].hasBeenPicked = true;
export const SelectedCorrectAnswer: Story = {
  args: {
    answers: storyAnswers
  }
};

storyAnswers = defaultAnswers();
storyAnswers.answers[0].isCorrectAnswer = true;
storyAnswers.answers[0].hasBeenPicked = true;
export const CompleteXS: Story = {
  args: {
    answers: storyAnswers
  },
  globals: { viewport: { value: 'viewport360', isRotated: false } },
  tags: ['viewport360']
};

storyAnswers = defaultAnswers();
storyAnswers.answers[0].isCorrectAnswer = true;
storyAnswers.answers[0].hasBeenPicked = true;
export const CompleteS: Story = {
  args: {
    answers: storyAnswers
  },
  globals: { viewport: { value: 'viewport568', isRotated: false } },
  tags: ['viewport568']
};

storyAnswers = defaultAnswers();
storyAnswers.answers[0].isCorrectAnswer = true;
storyAnswers.answers[0].hasBeenPicked = true;
export const CompleteTablet: Story = {
  args: {
    answers: storyAnswers
  },
  globals: { viewport: { value: 'viewport768', isRotated: false } },
  tags: ['viewport768']
};

storyAnswers = defaultAnswers();
storyAnswers.answers[0].isCorrectAnswer = true;
storyAnswers.answers[0].hasBeenPicked = true;
export const Complete: Story = {
  args: {
    answers: storyAnswers
  }
};

import { test, expect, type Page } from '@playwright/test';
import waitForStoryToBeReady from './waitForStoryToBeReady';
import screenshotName from './screenshotName';
import storiesFromStorybookManifest, { type IStory } from './storiesFromStorybookManifest';

const allStories = storiesFromStorybookManifest();

allStories.forEach((story: IStory) => {
  test.describe('story.file', () => {
    test(story.story, async ({ page, browserName }: { page: Page; browsername: string }) => {
      await page.goto(story.url);
      await waitForStoryToBeReady(page);
      if (story.width) {
        await page.setViewportSize({ width: story.width, height: 800 });
      }
      await expect(page).toHaveScreenshot(screenshotName(browserName, story.story), {
        fullPage: true,
        timeout: 60000
      });
    });
  });
});

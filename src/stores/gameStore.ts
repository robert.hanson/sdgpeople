import { ref, computed } from 'vue';
import { defineStore } from 'pinia';
import { msGraphGet } from '../utils/callMsGraph';
import { graphConfig } from '@/authconfig';
import randomizedList from '@/libs/randomizedList';
import type { Person } from '@/types/Person';
import { useRouter } from 'vue-router';
import { useFirebaseStore } from './firebaseStore';
import { useMsal } from '@/composition-api/useMsal';
import type { CashInRequest } from '@/types/CashInRequest';
import type { AllPoints } from '@/types/AllPoints';

export const useGameStore = defineStore('game', () => {
  // Local variables
  const router = useRouter();
  const firebaseStore = useFirebaseStore();

  // State variables
  const loading = ref(false);
  const loaded = ref(false);
  const errored = ref(false);
  const directory = ref<Person[]>([]);
  const users = ref<Person[]>([]);
  const auth = ref<any>({ uniqueId: '' });
  const pageNum = ref(0);
  const points = ref(0);
  const currentUserId = ref('');

  // getters
  const currentUser = computed(() => users.value.find((x) => x.id == currentUserId.value) || ({} as Person));
  const selectPerson = computed(() => (users.value.length > 0 ? users.value[0] : {}));
  const userCount = computed(() => users.value.length);
  const cashInPoints = computed(() => Math.floor(points.value / firebaseStore.config.pointsNeededForOneSdgBuck) * firebaseStore.config.pointsNeededForOneSdgBuck);
  const sdgBucksAvailable = computed(() => cashInPoints.value / firebaseStore.config.pointsNeededForOneSdgBuck);

  // actions
  async function next() {
    pageNum.value = pageNum.value + 1;
    router.push({ name: 'panel', query: { pageNum: pageNum.value } });
  }

  function setLoading(isLoading: boolean): void {
    loading.value = isLoading;
  }

  async function loadDirectory() {
    const ids = (await msGraphGet(graphConfig.directory.endpoint, auth.value.accessToken, 'json')).value;
    const promises = [];
    for (let i = 0; i < ids.length; i++) {
      const addr = graphConfig.user.endpoint.replace('user-id', ids[i].id);
      const promise = msGraphGet(addr, auth.value.accessToken, 'json').then((data) => directory.value.push(data));
      promises.push(promise);
    }
    await Promise.all(promises);
  }

  async function loadPerson(id: string): Promise<Person> {
    auth.value = await getAuth();
    const addr = graphConfig.user.endpoint.replace('user-id', id);
    const person = await msGraphGet(addr, auth.value.accessToken, 'json');
    person.image = await getImage(person);
    return person;
  }

  const getImage = async (person: Person): Promise<string> => {
    const maxRetries = 3;
    let attempt = 0;
    while (attempt < maxRetries) {
      const image = await msGraphGet(graphConfig.image.endpoint.replace('user-id', person.id), auth.value.accessToken, 'image');
      if (image !== null) {
        return image;
      }

      attempt++;
      if (attempt >= maxRetries) {
        return '';
      }
    }
    throw new Error('Failed to fetch image after multiple attempts');
  };

  const loadImages = async (users: Person[]) => {
    const promises = [];
    for (let i = 0; i < users.length; i++) {
      const u = users[i];
      const p = getImage(users[i]).then((img) => (u.image = img));
      promises.push(p);
    }
    await Promise.all(promises);
  };

  async function getAuth() {
    const { instance: msal } = useMsal();
    try {
      auth.value = await msal.acquireTokenSilent({
        scopes: graphConfig.directory.scopes
      });
    } catch (e) {
      try {
        auth.value = await msal.acquireTokenPopup({
          scopes: graphConfig.directory.scopes
        });
      } catch (e) {
        return { error: true };
      }
    }
    return auth.value;
  }

  async function loadData() {
    auth.value = await getAuth();
    if (auth.value.error) {
      errored.value = true;
      return;
    }
    currentUserId.value = auth.value.uniqueId;

    await firebaseStore.loadData(currentUserId.value);
    points.value = firebaseStore.points;

    const blacklist = firebaseStore.blacklist;
    await loadDirectory();
    users.value = directory.value.filter((person) => !blacklist.some((b: any) => b.id === person.id));
    await loadImages(users.value);
    sessionStorage.setItem('directory', JSON.stringify(directory.value));

    loaded.value = true;
  }

  function randomUsersList() {
    return randomizedList<Person>(users.value);
  }

  async function addOnePoint() {
    points.value = await firebaseStore.addOnePoint(currentUserId.value);
  }

  async function cashIn(): Promise<void> {
    await firebaseStore.cashIn(currentUserId.value, currentUser.value.displayName, cashInPoints.value, sdgBucksAvailable.value);
    await firebaseStore.loadData(currentUserId.value);
    points.value = firebaseStore.points;
  }

  async function processCashInRequests(requests: CashInRequest[]): Promise<void> {
    for (const request in requests) {
      await firebaseStore.processCashInRequest(requests[request]);
    }
  }

  async function leaderboard(): Promise<AllPoints[]> {
    return (await firebaseStore.leaderboard())
      .sort((a, b) => b.points - a.points)
      .slice(0, 10)
      .map((entry) => ({ id: entry.id, points: entry.points, person: directory.value.find((person) => person.id === entry.id)! }));
  }

  return {
    loaded,
    loading,
    errored,
    directory,
    users,
    userCount,
    next,
    selectPerson,
    randomUsersList,
    getImage,
    loadPerson,
    setLoading,
    loadData,
    points,
    addOnePoint,
    cashIn,
    cashInPoints,
    processCashInRequests,
    sdgBucksAvailable,
    currentUser,
    leaderboard
  };
});

import type { AdminList } from '@/types/AdminList';
import { initializeApp } from 'firebase/app';
import { getFirestore, doc, getDoc, setDoc, Firestore, collection, getDocs, deleteDoc } from 'firebase/firestore';
import type { Config } from '@/types/Config';
import type { CashInRequest } from '@/types/CashInRequest';
import type { AllPoints } from '@/types/AllPoints';
import type { Campaign } from '@/stores/updateDataCampaignStore';

let db: Firestore;
let app;

const firebaseConfig = {
  apiKey: 'AIzaSyBK7LV7jQiEgAFJnDj_VI3s78XIf5ExGQo',
  authDomain: 'sdgpeople.firebaseapp.com',
  databaseURL: 'https://sdgpeople.firebaseio.com',
  projectId: 'sdgpeople',
  storageBucket: 'sdgpeople.appspot.com',
  messagingSenderId: '342605692685',
  appId: '1:342605692685:web:15555d325a0069b6'
};

function initFirebase() {
  // Initialize Firebase
  app = initializeApp(firebaseConfig);
  db = getFirestore(app);
}

const getlist = async (collection: string, entry: string, key: string): Promise<any> => {
  const docRef = doc(db, collection, entry);
  const docSnap = await getDoc(docRef);
  return docSnap.exists() ? docSnap.data()[key] : [];
};

const setList = async (collection: string, entry: string, key: string, list: any): Promise<any> => {
  try {
    const docRef = doc(db, collection, entry);
    await setDoc(docRef, { [key]: list });
  } catch (e) {
    console.log(e);
  }
};

const blacklistCollection = 'blacklist';
const blacklistEntry = '_blacklist';
const blacklistKey = 'blacklisted';

const getBlacklist = async (): Promise<AdminList[]> => {
  return await getlist(blacklistCollection, blacklistEntry, blacklistKey);
};

const blacklistUpdate = async (blacklisted: AdminList[]) => {
  await setList(blacklistCollection, blacklistEntry, blacklistKey, blacklisted);
};

const adminCollection = 'admin';
const adminEntry = '_admin';
const adminkey = 'admin';

const getAdmins = async (): Promise<AdminList[]> => {
  return await getlist(adminCollection, adminEntry, adminkey);
};

const setAdmins = async (admins: AdminList[]) => {
  await setList(adminCollection, adminEntry, adminkey, admins);
};

const configCollection = 'config';
const configEntry = 'config';
const getConfig = async (): Promise<Config> => {
  const docRef = doc(db, configCollection, configEntry);
  const docSnap = await getDoc(docRef);
  return docSnap.exists() ? (docSnap.data() as Config) : { pointsNeededForOneSdgBuck: 1, maxBucksPerPerson: 10 };
};

const setConfig = async (config: Config) => {
  try {
    const docRef = doc(db, configCollection, configEntry);
    await setDoc(docRef, config);
  } catch (e) {
    console.log(e);
  }
};

const userPointsCollection = 'points';
const getPoints = async (currentUserId: string): Promise<number> => {
  if (currentUserId === '') {
    return 0;
  }
  const docRef = doc(db, userPointsCollection, currentUserId);
  const docSnap = await getDoc(docRef);
  return docSnap.exists() ? (docSnap.data().points as number) : 0;
};

const getAllPoints = async (): Promise<AllPoints[]> => {
  const snapshot = await collection(db, userPointsCollection);
  const list = await getDocs(snapshot);
  const docs = list.docs.map((doc) => ({ id: doc.id, ...doc.data() }) as AllPoints);
  return docs;
};

const addOnePoint = async (currentUserId: string) => {
  const docRef = doc(db, userPointsCollection, currentUserId);
  const docSnap = await getDoc(docRef);
  let currentPoints = 0;
  if (docSnap.exists()) {
    currentPoints = docSnap.data().points;
  }
  const newPoints = currentPoints + 1;
  await setDoc(docRef, { points: newPoints });
  return newPoints;
};

const subtractPoints = async (currentUserId: string, points: number) => {
  const docRef = doc(db, userPointsCollection, currentUserId);
  const docSnap = await getDoc(docRef);
  if (docSnap.exists()) {
    const currentPoints = docSnap.data().points;
    const newPoints = currentPoints - points;
    await setDoc(docRef, { points: newPoints });
    return newPoints;
  }
  return 0;
};

const cashInCollection = 'cashInTransactionLog';

const cashInTransaction = async (transaction: Record<string, string | number | boolean>) => {
  const docRef = doc(db, cashInCollection, transaction.date.toString());
  await setDoc(docRef, transaction);
};

const cashInTransactionList = async (): Promise<CashInRequest[]> => {
  const snapshot = await collection(db, cashInCollection);
  const list = await getDocs(snapshot);
  const docs = list.docs.map((doc) => doc.data());
  return docs as CashInRequest[];
};

const processCashInRequest = async (request: CashInRequest) => {
  const docRef = doc(db, cashInCollection, request.date.toString());
  await setDoc(docRef, request);
};

// campaigns
const campaignCollection = 'campaigns';
const getCampaigns = async (): Promise<Campaign[]> => {
  const snapshot = await collection(db, campaignCollection);
  const list = await getDocs(snapshot);
  const docs = list.docs.map((doc) => ({ ...doc.data() }) as Campaign);
  return docs;
};

const addCampaign = async (campaign: Campaign): Promise<void> => {
  const docRef = doc(db, campaignCollection, campaign.id.toString());
  await setDoc(docRef, campaign);
};

const removeCampaign = async (campaignName: string): Promise<void> => {
  const docRef = doc(db, campaignCollection, campaignName);
  await deleteDoc(docRef);
};

export { initFirebase, getBlacklist, blacklistUpdate, getAdmins, setAdmins, getConfig, setConfig, getPoints, getAllPoints, addOnePoint, subtractPoints, cashInTransaction, cashInTransactionList, processCashInRequest, getCampaigns, addCampaign, removeCampaign };

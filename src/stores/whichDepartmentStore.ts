import { defineStore } from 'pinia';
import { useGameStore } from '../stores/gameStore';
import { useDepartmentStore } from './departmentStore';
import type { Person } from '@/types/Person';
import randomizedList from '@/libs/randomizedList';
import answerButtonsFactory from '@/libs/AnswerButtonsFactory';
import type { Answers } from '@/types/Answers';

type State = {
  list: Person[];
};

export const useWhichDepartmentStore = defineStore('WhichDepartmentStore', {
  state: (): State => ({
    list: [] as Person[]
  }),
  getters: {
    users(state: State): Person[] {
      if (state.list.length === 0) {
        const gameStore = useGameStore();
        state.list = gameStore.randomUsersList();
      }
      return state.list;
    },
    person(): Person {
      return this.users[0];
    },
    answers(): Answers {
      const departmentStore = useDepartmentStore();
      const alternateAnswers = departmentStore.departments.filter((department) => department !== this.person.department).slice(0, 3);
      const allAnswers = randomizedList([...alternateAnswers, this.person.department]);
      const answerButtons = answerButtonsFactory(allAnswers, this.person.department);
      return answerButtons;
    }
  },

  actions: {
    removePerson(person: Person) {
      this.list = this.list.filter((x) => x.id !== person.id);
    },

    recyclePerson(person: Person) {
      this.removePerson(person);
      this.list.push(person);
    }
  }
});

import { createRouter, createWebHistory } from 'vue-router';
import HomeView from '../views/HomeView.vue';
import PanelView from '../views/PanelView.vue';
import AdminView from '../views/AdminView.vue';
import InviteView from '../views/InviteView.vue';
import EditYourProfileView from '../views/EditYourProfileView.vue';
import AboutPersonView from '../views/AboutPersonView.vue';
import DirectoryView from '../views/DirectoryView.vue';
import CashInView from '../views/CashInView.vue';
import LeaderBoardView from '../views/LeaderboardView.vue';
import UpdateDataCampaignView from '../views/UpdateDataCampaignView.vue';
import { useGameStore } from '@/stores/gameStore';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
      meta: { redirectToHome: false, hasHeader: false }
    },
    {
      path: '/Panel',
      name: 'panel',
      component: PanelView,
      meta: { redirectToHome: true, hasHeader: true },
      children: [
        {
          path: 'WhichDepartment',
          name: 'whichdepartment',
          component: () => import('../components/Panels/WhichDepartment.vue')
        },
        {
          path: 'WhoAmI',
          name: 'whoami',
          component: () => import('../components/Panels/WhoAmI.vue')
        }
      ]
    },
    {
      path: '/Admin',
      name: 'admin',
      component: AdminView,
      meta: { redirectToHome: true, hasHeader: false }
    },
    {
      path: '/Invite',
      name: 'invite',
      component: InviteView,
      meta: { redirectToHome: true, hasHeader: true }
    },
    {
      path: '/CashIn',
      name: 'cashin',
      component: CashInView,
      meta: { redirectToHome: true, hasHeader: true }
    },
    {
      path: '/AboutPerson',
      name: 'aboutperson',
      component: AboutPersonView,
      props: true,
      beforeEnter: (to, from) => {
        if (from.fullPath === '/') {
          return true;
        }
        window.open(to.fullPath, 'aboutperson');
        return false;
      },
      meta: { redirectToHome: false, hasHeader: false }
    },
    {
      path: '/EditYourProfile',
      name: 'edityourprofie',
      component: EditYourProfileView,
      meta: { redirectToHome: true, hasHeader: true }
    },
    {
      path: '/Directory',
      name: 'directory',
      component: DirectoryView,
      meta: { redirectToHome: true, hasHeader: true }
    },
    {
      path: '/LeaderBoard',
      name: 'leaderboard',
      component: LeaderBoardView,
      meta: { redirectToHome: true, hasHeader: true }
    },
    {
      path: '/UpdateDataCampaign',
      name: 'updateDataCampaign',
      component: UpdateDataCampaignView,
      meta: { redirectToHome: false, hasHeader: false }
    }
  ],
  scrollBehavior() {
    return { top: 0 };
  }
});

export default {
  install(app: any) {
    router.install(app);

    router.beforeEach((to) => {
      const gameStore = useGameStore();
      if (to.meta.redirectToHome && !gameStore.loaded) {
        return { name: 'home' };
      }
    });
  }
};

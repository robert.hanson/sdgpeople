import { defineStore } from 'pinia';
import { useGameStore } from '../stores/gameStore';
import type { Person } from '@/types/Person';

type State = {
  answerCount: number;
  list: Person[];
};

export const useMemoryGameStore = defineStore('memoryGameStore', {
  state: (): State => ({
    answerCount: 3,
    list: [] as Person[]
  }),
  getters: {
    users(state: State): Person[] {
      if (state.list.length < state.answerCount) {
        const gameStore = useGameStore();
        state.list = gameStore.randomUsersList().filter((person) => person.image !== '');
      }
      return state.list;
    },
    person(): Person {
      return this.users[0];
    },

    answers(state: State): Person[] {
      const answerList = this.users.slice(0, state.answerCount);
      state.list = state.list.slice(state.answerCount);
      return answerList;
    }
  },

  actions: {
    setAnswerCount(answers: number) {
      this.answerCount = answers;
    }
  }
});

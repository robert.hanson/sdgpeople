import { defineStore } from 'pinia';
import { useGameStore } from './gameStore';
import type { Person } from '@/types/Person';
import { useDepartmentStore } from './departmentStore';
import randomizedList from '@/libs/randomizedList';

export const useDepartmentPeopleStore = defineStore('DepartmentPeopleStore', () => {
  const departmentPeople = () => {
    const departmentStore = useDepartmentStore();
    const gameStore = useGameStore();

    const department = randomizedList(departmentStore.departments)[0];
    const people = gameStore.users.filter((person) => person.department === department).sort((a: Person, b: Person) => (a.displayName < b.displayName ? -1 : 1));
    return { department, people };
  };

  const next = () => {
    const gameStore = useGameStore();
    gameStore.next();
  };

  return { departmentPeople, next };
});

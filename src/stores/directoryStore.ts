import { defineStore } from 'pinia';
import { useGameStore } from './gameStore';
import type { Person } from '@/types/Person';
import { computed, ref } from 'vue';
import type { SortByOptionKeys, SortByOptions } from '@/types/Directory';

export const useDirectoryStore = defineStore('DirectoryStore', () => {
  const gameStore = useGameStore();
  const department = ref('Java');
  const sortByOptions: SortByOptions = [
    { label: 'Alphabetical', value: 'displayName' },
    { label: 'Hire Date', value: 'employeeHireDate' }
  ];
  const sortBy = ref<SortByOptionKeys>(sortByOptions[0].value);
  const departments = [...new Set(gameStore.users.map((user: Person) => user.department))];

  function setDepartment(value: string) {
    department.value = value;
  }

  function setSortBy(value: SortByOptionKeys) {
    sortBy.value = value;
  }

  const users = computed(() => {
    return gameStore.users
      .filter((person: Person) => person.department === department.value)
      .sort((a: Person, b: Person) => {
        if (sortBy.value === 'employeeHireDate') {
          const dateA = new Date(a[sortBy.value]).getTime();
          const dateB = new Date(b[sortBy.value]).getTime();

          return dateA - dateB;
        }

        const valueA = a[sortBy.value]?.toLowerCase() || '';
        const valueB = b[sortBy.value]?.toLowerCase() || '';

        return valueA.localeCompare(valueB);
      });
  });

  return { department, departments, sortBy, setDepartment, setSortBy, sortByOptions, users };
});

import type { Meta, StoryObj } from '@storybook/vue3';
import { action } from '@storybook/addon-actions';
import MemoryGameView from './MemoryGameView.vue';
import type { Person } from '@/types/Person';

const people = ['adam_johnson', 'anh_duong', 'ben_fischer', 'ben_stori', 'gucci_aguh', 'kate_williamson', 'lindsey_hansen', 'nick_bruecken', 'cooper_miller'];
const defaultAnswers = () => people.slice(0, 4).map((person) => ({ displayName: person.replace('_', ' '), image: `/${person}.jpg` })) as Person[];

// More on how to set up stories at: https://storybook.js.org/docs/writing-stories
const meta = {
  component: MemoryGameView,
  // This component will have an automatically generated docsPage entry: https://storybook.js.org/docs/writing-docs/autodocs
  tags: ['autodocs'],
  args: {
    answers: defaultAnswers(),
    showPlusOne: false,
    completed: false
  },
  render: (args: any) => ({
    components: { MemoryGameView },
    setup() {
      return { args };
    },
    methods: {
      onAwardPooints: action('onAwardPoints'),
      onCorrectAnswerClick: action('onCorrectAnswerClick'),
      onNextClick: action('onNextClick')
    },
    template: '<MemoryGameView v-bind="args" @onCorrectAnswerClick="onCorrectAnswerClick" @onNextClick="onNextClick"/>'
  })
} satisfies Meta<typeof MemoryGameView>;

export default meta;
type Story = StoryObj<typeof meta>;
/*
 *👇 Render functions are a framework specific feature to allow you control on how the component renders.
 * See https://storybook.js.org/docs/api/csf
 * to learn how to use render functions.
 */
let answers = defaultAnswers();
export const Playground: Story = {
  args: { ...meta.args, answers, completed: false }
};

answers = defaultAnswers();
export const Flipped: Story = {
  args: { ...meta.args, answers, completed: true }
};

answers = defaultAnswers();
export const PickedCorrectAnswer: Story = {
  args: { ...meta.args, answers, showPlusOne: true, completed: true }
};

answers = defaultAnswers();
export const Narrow: Story = {
  args: { ...meta.args, answers, showPlusOne: true, completed: true },
  globals: { viewport: { value: 'viewport360', isRotated: false } },
  tags: ['viewport360']
};

answers = defaultAnswers();
export const Tablet: Story = {
  args: { ...meta.args, answers, showPlusOne: true, completed: true },
  globals: { viewport: { value: 'viewport768', isRotated: false } },
  tags: ['viewport768']
};

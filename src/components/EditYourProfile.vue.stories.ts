import type { Meta, StoryObj } from '@storybook/vue3';
import '../assets/main.css';
import EditYourProfile from './EditYourProfile.vue';
import person from '../stories/fakeData/person.json';
import { vueRouter } from 'storybook-vue3-router';
import type { RouteRecordRaw } from 'vue-router';
const customRoutes = [
  {
    path: '/',
    name: 'aboutperson'
  }
] as RouteRecordRaw[];

const meta = {
  component: EditYourProfile,
  tags: ['autodocs'],
  args: {
    person
  },
  render: (args: any) => ({
    components: { EditYourProfile },
    setup() {
      return { args };
    },
    template: '<EditYourProfile v-bind="args" @onAnswerClick="handleClick" />'
  }),
  decorators: [vueRouter(customRoutes)]
} satisfies Meta<typeof EditYourProfile>;

export default meta;

type Story = StoryObj<typeof meta>;
/*
 *👇 Render functions are a framework specific feature to allow you control on how the component renders.
 * See https://storybook.js.org/docs/api/csf
 * to learn how to use render functions.
 */
export const Playground: Story = {};

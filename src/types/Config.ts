interface Config {
  pointsNeededForOneSdgBuck: number;
  maxBucksPerPerson: number;
}
export type { Config };

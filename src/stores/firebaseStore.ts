import { defineStore } from 'pinia';
import * as firebase from '../libs/firebase';
import { ref } from 'vue';
import type { Config } from '@/types/Config';
import type { AdminList } from '@/types/AdminList';
import type { CashInRequest } from '@/types/CashInRequest';
import type { AllPoints } from '@/types/AllPoints';
import type { Campaign } from './updateDataCampaignStore';
export const useFirebaseStore = defineStore('firebase', () => {
  // State variables

  const admins = ref<AdminList[]>([]);
  const blacklist = ref<AdminList[]>([]);
  const config = ref<Config>({ pointsNeededForOneSdgBuck: 0, maxBucksPerPerson: 0 });
  const points = ref<number>(0);

  // actions
  async function loadData(currentUserId: string) {
    firebase.initFirebase();
    admins.value = await firebase.getAdmins();
    blacklist.value = await firebase.getBlacklist();
    config.value = await firebase.getConfig();
    points.value = await firebase.getPoints(currentUserId);
  }

  async function saveBlacklist(blacklist: AdminList[]) {
    await firebase.blacklistUpdate(blacklist);
  }

  async function saveAdmins(blacklist: AdminList[]) {
    await firebase.setAdmins(blacklist);
  }

  async function saveConfig(config: Config) {
    await firebase.setConfig(config);
  }

  async function addOnePoint(currentUserId: string): Promise<number> {
    const newPoints = await firebase.addOnePoint(currentUserId);
    return newPoints;
  }

  async function cashIn(currentUserId: string, displayName: string, cashInPoints: number, sdgBucksAvailable: number): Promise<number> {
    const transaction = {
      userId: currentUserId,
      name: displayName,
      points: cashInPoints,
      sdgBucks: sdgBucksAvailable,
      date: Date.now(),
      credited: false
    };
    await firebase.cashInTransaction(transaction);
    const newPoints = await firebase.subtractPoints(currentUserId, cashInPoints);
    return newPoints;
  }

  async function cashInTransactionList(): Promise<CashInRequest[]> {
    return await firebase.cashInTransactionList();
  }

  async function processCashInRequest(request: CashInRequest): Promise<void> {
    request.process = false;
    request.credited = true;
    request.creditDate = Date.now();
    await firebase.processCashInRequest(request);
  }

  async function leaderboard(): Promise<AllPoints[]> {
    return await firebase.getAllPoints();
  }

  // campaigns

  async function getCampaigns(): Promise<Campaign[]> {
    return await firebase.getCampaigns();
  }

  async function addCampaign(campaign: Campaign): Promise<void> {
    await firebase.addCampaign(campaign);
  }

  async function removeCampaign(campaignName: string): Promise<void> {
    await firebase.removeCampaign(campaignName);
  }

  return { loadData, admins, blacklist, config, saveBlacklist, saveAdmins, saveConfig, points, addOnePoint, cashIn, cashInTransactionList, processCashInRequest, leaderboard, getCampaigns, addCampaign, removeCampaign };
});

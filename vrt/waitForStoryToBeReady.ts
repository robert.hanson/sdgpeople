import { type Page } from '@playwright/test'

async function waitForStoryToBeReady(page: Page) {
  const additionalWaitTime = 10000
  await page.waitForLoadState('domcontentloaded')
  await page.waitForSelector('#storybook-root')
  await page.waitForTimeout(additionalWaitTime)
}

export default waitForStoryToBeReady

import { defineStore } from 'pinia';
import { useGameStore } from './gameStore';
import type { Person } from '@/types/Person';
import { computed, ref } from 'vue';

export const useDepartmentStore = defineStore('departmentStore', () => {
  // state
  const departmentList = ref<string[]>([]);

  // getters
  const departments = computed(() => {
    if (departmentList.value.length === 0) {
      const gameStore = useGameStore();
      const depts = gameStore.users.reduce((accum: Set<string>, person: Person) => {
        accum.add(person.department);
        return accum;
      }, new Set());
      departmentList.value = [...depts.values()];
    }
    return departmentList.value;
  });

  return { departments };
});

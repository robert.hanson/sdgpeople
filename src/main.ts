import './assets/main.css';

import { createApp } from 'vue';
import { createPinia } from 'pinia';
import { Vue3Mq } from 'vue3-mq';

import App from './App.vue';
import router from './router';
import { msalPlugin } from './plugins/msalPlugin';
import { msalInstance } from './authconfig';
import { type AuthenticationResult, EventType } from '@azure/msal-browser';

// Account selection logic is app dependent. Adjust as needed for different use cases.
const accounts = msalInstance.getAllAccounts();
if (accounts.length > 0) {
  msalInstance.setActiveAccount(accounts[0]);
}
msalInstance.addEventCallback((event) => {
  if (event.eventType === EventType.LOGIN_SUCCESS && event.payload) {
    const payload = event.payload as AuthenticationResult;
    const account = payload.account;
    msalInstance.setActiveAccount(account);
  }
});

const app = createApp(App);

app.use(createPinia());
app.use(msalPlugin, msalInstance);
await msalInstance.initialize();
app.use(router, app as any);

app.use(Vue3Mq, {
  preset: 'tailwind'
});

app.mount('#app');

import type { Meta, StoryObj } from '@storybook/vue3';
import { action } from '@storybook/addon-actions';
import '../assets/main.css';
import AnswerButton from './AnswerButton.vue';

const defaultAnswer = {
  buttonText: 'Jackson Oppenheim',
  isCorrectAnswer: false,
  hasBeenPicked: false
};

const meta = {
  component: AnswerButton,
  tags: ['autodocs'],
  args: {
    answer: defaultAnswer
  },
  render: (args: any) => ({
    components: { AnswerButton },
    setup() {
      return { args };
    },
    methods: {
      handleClick: action('button-clicked')
    },
    template: '<AnswerButton v-bind="args" @onAnswerClick="handleClick" />'
  })
} satisfies Meta<typeof AnswerButton>;

export default meta;
type Story = StoryObj<typeof meta>;
/*
 *👇 Render functions are a framework specific feature to allow you control on how the component renders.
 * See https://storybook.js.org/docs/api/csf
 * to learn how to use render functions.
 */
export const Playground: Story = {
  args: {
    answer: defaultAnswer
  }
};

export const CorrectAnswerPicked: Story = {
  args: {
    answer: { ...defaultAnswer, isCorrectAnswer: true, hasBeenPicked: true }
  }
};

export const IncorrectAnswerPicked: Story = {
  args: {
    answer: { ...defaultAnswer, isCorrectAnswer: false, hasBeenPicked: true }
  }
};

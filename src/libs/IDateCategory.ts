enum Year {
  Unknown = -5,
  LessThan3Months,
  LessThan6Months,
  LessThan1Year
}

interface IDateCategory {
  days: number;
  year: number;
  employeeHireDate: string;
}

export { type IDateCategory, Year };

# sdgpeople

# Contributing

## Machine setup

This project uses bun, a super-fast replacement for npm. Install bun from https://bun.sh/docs/installation#windows (try npm install -g bun )

## Source Repository

https://gitlab.com/robert.hanson/sdgpeople

## Making a code change

1 create a branch off of master
2 make your changes.

- Changes require the following:

* Unit tests for changed code.
* Storybook stories for new components. See the section below on storybook
* running visual regression testing (see the section on visual regression testing below).
* running "npm run preCommit"

## Creating a merge request

1 Commit your changes and push your branch to the repository.
2 Create a merge request
-- visit https://gitlab.com/robert.hanson/sdgpeople/-/merge_requests
-- Click "Create merge request"
-- Fill in the title and desription if you need to
-- Be sure to check the "delete source branch" and "squash commits" checkboxes
-- Click the "Create merge request" button.
3 post in the #sdgpeople slack channel, a link to your merge request.
4 Someone will review it and merge it in.

## Deployment

Merges to main are automatically deployed. You can check the status at https://gitlab.com/robert.hanson/sdgpeople ("build / pipelines")

# Admin

Select people are admins. If you wish to be an admin, contact Kyle.

Admin users have an additional "admin" button on the home page.

# Development Notes (generated during initial vue installation)

This template should help get you started developing with Vue 3 in Vite.

## Recommended IDE Setup

[VSCode](https://code.visualstudio.com/) + [Volar](https://marketplace.visualstudio.com/items?itemName=Vue.volar) (and disable Vetur) + [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin).

## Type Support for `.vue` Imports in TS

TypeScript cannot handle type information for `.vue` imports by default, so we replace the `tsc` CLI with `vue-tsc` for type checking. In editors, we need [TypeScript Vue Plugin (Volar)](https://marketplace.visualstudio.com/items?itemName=Vue.vscode-typescript-vue-plugin) to make the TypeScript language service aware of `.vue` types.

If the standalone TypeScript plugin doesn't feel fast enough to you, Volar has also implemented a [Take Over Mode](https://github.com/johnsoncodehk/volar/discussions/471#discussioncomment-1361669) that is more performant. You can enable it by the following steps:

1. Disable the built-in TypeScript Extension
   1. Run `Extensions: Show Built-in Extensions` from VSCode's command palette
   2. Find `TypeScript and JavaScript Language Features`, right click and select `Disable (Workspace)`
2. Reload the VSCode window by running `Developer: Reload Window` from the command palette.

## Customize configuration

See [Vite Configuration Reference](https://vitejs.dev/config/).

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Run Unit Tests with [Vitest](https://vitest.dev/)

```sh
npm run test:unit
```

### Run End-to-End Tests with [Playwright](https://playwright.dev)

```sh
# Install browsers for the first run
npx playwright install

# When testing on CI, must build the project first
npm run build

# Runs the end-to-end tests
npm run test:e2e
# Runs the tests only on Chromium
npm run test:e2e -- --project=chromium
# Runs the tests of a specific file
npm run test:e2e -- tests/example.spec.ts
# Runs the tests in debug mode
npm run test:e2e -- --debug
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```

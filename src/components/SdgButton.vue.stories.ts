import type { Meta, StoryObj } from '@storybook/vue3';
import { action } from '@storybook/addon-actions';
import '../assets/main.css';
import SdgButton from './SdgButton.vue';
const meta = {
  component: SdgButton,
  tags: ['autodocs'],
  args: {
    color: 'sdg-blue'
  },
  render: (args: any) => ({
    components: { SdgButton },
    setup() {
      return { args };
    },
    methods: {
      handleClick: action('button-clicked')
    },
    template: '<SdgButton v-bind="args" @onClick="handleClick">Button Label</SdgButton>'
  })
} satisfies Meta<typeof SdgButton>;

export default meta;
type Story = StoryObj<typeof meta>;
/*
 *👇 Render functions are a framework specific feature to allow you control on how the component renders.
 * See https://storybook.js.org/docs/api/csf
 * to learn how to use render functions.
 */
export const Playground: Story = {
  args: {
    color: 'bg-sdg-blue'
  }
};

export const Green: Story = {
  args: {
    color: 'bg-green-900'
  }
};

export const Red: Story = {
  args: {
    color: 'bg-red-900'
  }
};

import type { Meta, StoryObj } from '@storybook/vue3';
import type { Person } from '@/types/Person';
import '../assets/main.css';
import SdgProfilePanel from './SdgProfilePanel.vue';
import people from '../stories/fakeData/people.json';
import { within } from '@storybook/test';

const meta = {
  component: SdgProfilePanel,
  tags: ['autodocs'],
  args: {},
  render: (args: any) => ({
    components: { SdgProfilePanel },
    setup() {
      return { args };
    },
    template: '<div style="background-color:gray;padding: 32px;"><SdgProfilePanel v-bind="args" /></div>'
  })
} satisfies Meta<typeof SdgProfilePanel>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Playground: Story = {
  args: {
    person: {
      ...people[0],
      image: '/cooper_miller.jpg',
      aboutMe: `I'm SDG's Recruiting Manager. I've been here for almost 7 years. I live in Orono with my husband, 2 sweet boys, and dog! My hobbies include: watching MN sports, (I'm a Vikings fan through and through, and get my heart broken every year!), reading, traveling, being outdoors, and enjoying some delicious pizza + wine.`
    } as Person
  }
};

export const NoBio: Story = {
  args: {
    person: {
      ...people[0],
      image: '/cooper_miller.jpg',
      aboutMe: ''
    } as Person
  }
};

export const ShortBio: Story = {
  args: {
    person: {
      ...people[0],
      image: '/cooper_miller.jpg',
      aboutMe: `I'm SDG's Recruiting Manager.`
    } as Person
  }
};

export const ExpandedBio: Story = {
  args: {
    person: {
      ...people[0],
      image: '/cooper_miller.jpg',
      aboutMe: `I'm SDG's Recruiting Manager. I've been here for almost 7 years. I live in Orono with my husband, 2 sweet boys, and dog! My hobbies include: watching MN sports, (I'm a Vikings fan through and through, and get my heart broken every year!), reading, traveling, being outdoors, and enjoying some delicious pizza + wine.`
    } as Person
  },
  play: async ({ canvasElement }: any) => {
    const canvas = within(canvasElement);
    (await canvas.findByText('See More')).click();
  }
};

export const NoImage: Story = {
  args: {
    person: {
      ...people[0],
      image: '',
      aboutMe: `I'm SDG's Recruiting Manager. I've been here for almost 7 years. I live in Orono with my husband, 2 sweet boys, and dog! My hobbies include: watching MN sports, (I'm a Vikings fan through and through, and get my heart broken every year!), reading, traveling, being outdoors, and enjoying some delicious pizza + wine.`
    } as Person
  }
};

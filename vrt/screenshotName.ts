const screenshotName = (browserName: string, story: string) => {
  return `${browserName}--${story}}.png`
}

export default screenshotName

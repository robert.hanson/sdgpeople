import { defineStore } from 'pinia';
import { useGameStore } from '../stores/gameStore';
import type { Person } from '@/types/Person';
import randomizedList from '@/libs/randomizedList';
import answerButtonsFactory from '@/libs/AnswerButtonsFactory';
import type { Answers } from '@/types/Answers';

type State = {
  list: Person[];
};
export const useWhoAmIStore = defineStore('whoAmIStore', {
  state: (): State => ({
    list: [] as Person[]
  }),
  getters: {
    users(state: State): Person[] {
      if (state.list.length === 0) {
        const gameStore = useGameStore();
        state.list = gameStore.randomUsersList().filter((person) => person.image !== '');
      }
      return state.list;
    },
    person(): Person {
      return this.users[0];
    },

    answers(): Answers {
      const gameStore = useGameStore();
      const alternateAnswers = gameStore
        .randomUsersList()
        .filter((x) => x.id !== this.person.id)
        .slice(0, 3);
      const allAnswers = randomizedList([...alternateAnswers, this.person]).map((person) => person.displayName);
      const answerButtons = answerButtonsFactory(allAnswers, this.person.displayName);
      return answerButtons;
    }
  },

  actions: {
    removePerson(person: Person) {
      this.list = this.list.filter((x) => x.id !== person.id);
    },

    recyclePerson(person: Person) {
      this.removePerson(person);
      this.list.push(person);
    }
  }
});

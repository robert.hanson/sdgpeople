import { LogLevel, PublicClientApplication } from '@azure/msal-browser';

// Config object to be passed to Msal on creation
export const msalConfig = {
  auth: {
    clientId: '8f7a19cf-8ad5-4bde-b41c-0701c35c8c97',
    authority: 'https://login.microsoftonline.com/316d3d07-900c-4e3d-99cb-215902dffe90/',
    requireAuthOnInitialize: true,
    autoRefreshToken: true
    // redirectUri: '/', // Must be registered as a SPA redirectURI on your app registration
    // postLogoutRedirectUri: '/' // Must be registered as a SPA redirectURI on your app registration
  },
  cache: {
    cacheLocation: 'localStorage'
  },
  system: {
    loggerOptions: {
      loggerCallback: (level: LogLevel, message: string, containsPii: boolean) => {
        if (containsPii) {
          return;
        }
        switch (level) {
          case LogLevel.Error:
            console.error(message);
            return;
          case LogLevel.Info:
            console.info(message);
            return;
          case LogLevel.Verbose:
            console.debug(message);
            return;
          case LogLevel.Warning:
            console.warn(message);
            return;
          default:
            return;
        }
      },
      logLevel: LogLevel.Verbose
    }
  }
};

export const msalInstance = new PublicClientApplication(msalConfig);

// Add here scopes for id token to be used at MS Identity Platform endpoints.
export const loginRequest = {
  scopes: ['User.Read']
};

const keys = [
  'aboutMe',
  'accountEnabled',
  // 'ageGroup',
  // 'assignedLicenses',
  // 'assignedPlans',
  'birthday',
  // 'businessPhones',
  // 'city',
  // 'companyName',
  // 'consentProvidedForMinor',
  // 'country',
  // 'createdDateTime',
  'department',
  'displayName',
  // 'employeeId',
  // 'faxNumber',
  // 'givenName',
  'employeeHireDate',
  'id',
  // 'imAddresses',
  'interests',
  'isResourceAccount',
  'jobTitle',
  // 'legalAgeGroupClassification',
  // 'licenseAssignmentStates',
  'mail',
  // 'mailboxSettings',
  // 'mailNickname',
  // 'mobilePhone',
  // 'mySite',
  'officeLocation',
  // 'onPremisesDistinguishedName',
  // 'onPremisesDomainName',
  // 'onPremisesExtensionAttributes',
  // 'onPremisesImmutableId',
  // 'onPremisesLastSyncDateTime',
  // 'onPremisesProvisioningErrors',
  // 'onPremisesSamAccountName',
  // 'onPremisesSecurityIdentifier',
  // 'onPremisesSyncEnabled',
  // 'onPremisesUserPrincipalName',
  // 'otherMails',
  // 'passwordPolicies',
  // 'passwordProfile',
  'pastProjects',
  // 'postalCode',
  // 'preferredDataLocation',
  // 'preferredLanguage',
  // 'preferredName',
  // 'provisionedPlans',
  // 'proxyAddresses',
  'responsibilities',
  'schools',
  // 'showInAddressList',
  'skills',
  // 'signInSessionsValidFromDateTime',
  // 'state',
  // 'streetAddress',
  // 'surname',
  // 'usageLocation',
  // 'userPrincipalName',
  'userType'
];
const props = keys.join(',');

// Add here the endpoints for MS Graph API services you would like to use.
export const graphConfig = {
  directory: {
    scopes: ['Directory.Read.All'],
    endpoint: `https://graph.microsoft.com/v1.0/users?$top=500`
  },
  user: {
    scopes: ['User.ReadBasic.All'],
    endpoint: `https://graph.microsoft.com/v1.0/users/user-id?$select=${props}`
  },
  image: {
    scopes: ['User.Read.All'],
    endpoint: 'https://graph.microsoft.com/v1.0/users/user-id/photo/$value',
    // endpoint: 'https://graph.microsoft.com/v1.0/me/photo',
    contentType: 'image/jpg'
  }
};

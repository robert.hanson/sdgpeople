import WhichDepartment from '../components/Panels/WhichDepartment.vue';
import WhoAmI from '../components/Panels/WhoAmI.vue';
import MemoryGame from '../components/Panels/MemoryGame.vue';

const panels = [
  { component: WhichDepartment, relativeFrequency: 50 },
  { component: WhoAmI, relativeFrequency: 100 },
  { component: MemoryGame, relativeFrequency: 20 }
];

const total = panels.reduce((accum, panel) => {
  accum += panel.relativeFrequency;
  return accum;
}, 0);

const ranges = (() => {
  let runningTotal = 0;
  return panels.map((panel) => {
    runningTotal += panel.relativeFrequency;
    return { panel, range: runningTotal / total };
  });
})();

const panelName = () => {
  const n = Math.random();
  const range = ranges.find((p) => n <= p.range) || ranges[0];
  return range.panel.component;
};

export default panelName;

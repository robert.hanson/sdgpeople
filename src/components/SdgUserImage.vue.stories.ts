import type { Meta, StoryObj } from '@storybook/vue3';
import '../assets/main.css';
import SdgUserImage from './SdgUserImage.vue';
import type { Person } from '@/types/Person';
import { vueRouter } from 'storybook-vue3-router';
import type { RouteRecordRaw } from 'vue-router';

const customRoutes = [
  {
    path: '/',
    name: 'aboutperson'
  }
] as RouteRecordRaw[];

const meta = {
  component: SdgUserImage,
  tags: ['autodocs'],
  args: { person: { image: '/cooper_miller.jpg' } as Person, degrees: 33 },
  render: (args: any) => ({
    components: { SdgUserImage },
    setup() {
      return { args };
    },
    template: '<SdgUserImage v-bind="args"></SdgUserImage>'
  }),
  decorators: [vueRouter(customRoutes)]
} satisfies Meta<typeof SdgUserImage>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Playground: Story = {
  args: { ...meta.args }
};

export const SmallImage: Story = {
  args: { ...meta.args, width: 150 }
};

export const MediumImage: Story = {
  args: { ...meta.args, width: 200 }
};

export const MediumImageWithCheckmark: Story = {
  args: { ...meta.args, width: 200, hasCheckmark: true }
};

export const LargeImage: Story = {
  args: { ...meta.args, width: 300 }
};

export const LargeImageWithCheckmark: Story = {
  args: { ...meta.args, width: 300, hasCheckmark: true }
};

export const XLargeImage: Story = {
  args: { ...meta.args, width: 400 }
};

export const XLargeImageWithCheckmark: Story = {
  args: { ...meta.args, width: 400, hasCheckmark: true }
};

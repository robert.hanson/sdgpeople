import type { Meta, StoryObj } from '@storybook/vue3';
import '../assets/main.css';
import TabBar from './TabBar.vue';

const meta = {
  component: TabBar,
  tags: ['autodocs'],
  args: {
    tabs: ['JavaScript', 'Java', 'Analysis', 'Operations'],
    activeTab: 'Analysis'
  },
  render: (args: any) => ({
    components: { TabBar },
    setup() {
      return { args };
    },
    template: '<TabBar v-bind="args" @update:activeTab="args.activeTab = $event" />'
  })
} satisfies Meta<typeof TabBar>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Playground: Story = {};

import type { Meta, StoryObj } from '@storybook/vue3';
import { action } from '@storybook/addon-actions';
import WhoAmIView from './WhoAmIView.vue';
import person from '../../stories/fakeData/person.json';
import defaultAnswers from '../defaultAnswers';
import { vueRouter } from 'storybook-vue3-router';
import type { RouteRecordRaw } from 'vue-router';
const customRoutes = [
  {
    path: '/',
    name: 'aboutperson'
  }
] as RouteRecordRaw[];

// More on how to set up stories at: https://storybook.js.org/docs/writing-stories
const meta = {
  component: WhoAmIView,
  // This component will have an automatically generated docsPage entry: https://storybook.js.org/docs/writing-docs/autodocs
  tags: ['autodocs'],
  args: {
    person,
    answers: defaultAnswers(),
    showPlusOne: false,
    completed: false
  },
  render: (args: any) => ({
    components: { WhoAmIView },
    setup() {
      return { args };
    },
    methods: {
      onAwardPooints: action('onAwardPoints'),
      onCorrectAnswerClick: action('onCorrectAnswerClick'),
      onNextClick: action('onNextClick')
    },
    template: '<WhoAmIView v-bind="args" @onAwardPoints="onAwardPoints" @onCorrectAnswerClick="onCorrectAnswerClick" @onNextClick="onNextClick"/>'
  }),
  decorators: [vueRouter(customRoutes)]
} satisfies Meta<typeof WhoAmIView>;

export default meta;
type Story = StoryObj<typeof meta>;
/*
 *👇 Render functions are a framework specific feature to allow you control on how the component renders.
 * See https://storybook.js.org/docs/api/csf
 * to learn how to use render functions.
 */
let answers = defaultAnswers();
answers.answers[0].isCorrectAnswer = true;
export const Playground: Story = {
  args: { ...meta.args, answers, completed: false }
};

answers = defaultAnswers();
answers.answers[0].isCorrectAnswer = true;
answers.answers[0].hasBeenPicked = true;
export const PickedCorrectAnswer: Story = {
  args: { ...meta.args, answers, showPlusOne: false, completed: false }
};

answers = defaultAnswers();
answers.answers[0].isCorrectAnswer = true;
answers.answers[0].hasBeenPicked = true;
export const PickedCorrectAnswerPlusOne: Story = {
  args: { ...meta.args, answers, showPlusOne: true, completed: false }
};

answers = defaultAnswers();
answers.answers[0].isCorrectAnswer = true;
answers.answers[0].hasBeenPicked = true;
export const PickedCorrectAnswerComplete: Story = {
  args: { ...meta.args, answers, completed: true }
};

answers = defaultAnswers();
answers.answers[0].isCorrectAnswer = true;
answers.answers[0].hasBeenPicked = false;
export const PickedCorrectAnswerWithBalloon: Story = {
  args: { ...meta.args, answers, completed: false, effect: 0 }
};
PickedCorrectAnswerWithBalloon.play = async (context: any) => {
  await new Promise((r) => setTimeout(r, 700));
  context.args.answers.answers[0].hasBeenPicked = true;
  context.args.showPlusOne = true;
  await new Promise((r) => setTimeout(r, 700));
  context.args.completed = true;
};

answers = defaultAnswers();
answers.answers[0].isCorrectAnswer = true;
answers.answers[0].hasBeenPicked = false;
export const PickedCorrectAnswerWithFireworks: Story = {
  args: { ...meta.args, answers, completed: false, effect: 1 }
};
PickedCorrectAnswerWithFireworks.play = async (context: any) => {
  await new Promise((r) => setTimeout(r, 700));
  context.args.answers.answers[0].hasBeenPicked = true;
  context.args.showPlusOne = true;
  await new Promise((r) => setTimeout(r, 700));
  context.args.completed = true;
};

answers = defaultAnswers();
answers.answers[0].isCorrectAnswer = true;
answers.answers[0].hasBeenPicked = false;
export const PickedCorrectAnswerWithPlusOne: Story = {
  args: { ...meta.args, answers, completed: false, effect: 2 }
};
PickedCorrectAnswerWithPlusOne.play = async (context: any) => {
  await new Promise((r) => setTimeout(r, 700));
  context.args.answers.answers[0].hasBeenPicked = true;
  context.args.showPlusOne = true;
  await new Promise((r) => setTimeout(r, 700));
  context.args.completed = true;
};

answers = defaultAnswers();
answers.answers[0].isCorrectAnswer = true;
answers.answers[0].hasBeenPicked = false;
export const PickedCorrectAnswerWithBubbles: Story = {
  args: { ...meta.args, answers, completed: false, effect: 3 }
};
PickedCorrectAnswerWithBubbles.play = async (context: any) => {
  await new Promise((r) => setTimeout(r, 700));
  context.args.answers.answers[0].hasBeenPicked = true;
  context.args.showPlusOne = true;
  await new Promise((r) => setTimeout(r, 700));
  context.args.completed = true;
};
answers = defaultAnswers();
answers.answers[0].isCorrectAnswer = false;
answers.answers[0].hasBeenPicked = true;
export const PickedIncorrectAnswer: Story = {
  args: { ...meta.args, answers, completed: false }
};

import type { Person } from './Person';

interface AllPoints {
  id: string;
  points: number;
  person: Person;
}

export type { AllPoints };

import type { Meta, StoryObj } from '@storybook/vue3';

import CorrectAnswerEffect from './CorrectAnswerEffect.vue';

// More on how to set up stories at: https://storybook.js.org/docs/writing-stories
const meta = {
  component: CorrectAnswerEffect,
  // This component will have an automatically generated docsPage entry: https://storybook.js.org/docs/writing-docs/autodocs
  tags: ['autodocs'],
  args: {
    effect: 0,
    effectRange: null,
    appearance: 0,
    appearanceRange: null,
    noAnimation: false
  },
  render: (args: any) => ({
    components: { CorrectAnswerEffect },
    setup() {
      return { args };
    },
    template: `
    <div>
      &nbsp;
      <CorrectAnswerEffect v-bind="args">+1</CorrectAnswerEffect>
    </div>`
  })
} satisfies Meta<typeof CorrectAnswerEffect>;

export default meta;
type Story = StoryObj<typeof meta>;
export const Playground: Story = {
  args: {
    appearance: 0,
    noAnimation: true
  }
};

export const Balloon: Story = {
  args: {
    effect: 0,
    noAnimation: true
  }
};

export const Fireworks: Story = {
  args: {
    effect: 1,
    noAnimation: true
  }
};

export const Rotation: Story = {
  args: {
    effect: 2,
    noAnimation: true
  }
};

export const Bubbles: Story = {
  args: {
    effect: 3,
    noAnimation: true
  }
};

export const FireworksHiddenOnSmall: Story = {
  args: {
    effect: 1,
    noAnimation: true
  },
  globals: { viewport: { value: 'viewport360', isRotated: false } },
  tags: ['viewport360']
};

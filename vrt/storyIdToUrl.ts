const storyIdToUrl = (storyId: string, args: string = ''): string => {
  const argsValue = args ? `args=${args}&` : ''
  const url = `http://127.0.0.1:8080/iframe.html?${argsValue}id=${storyId}&viewMode=story`
  return url
}
export default storyIdToUrl

interface Person {
  id: string;
  mail: string;
  displayName: string;
  jobTitle: string;
  department: string;
  userType: string;
  image: string;
  aboutMe: string;
  interests: string[];
  employeeHireDate: string;
  skills: string[];
  responsibilities: string[];
  birthday: string;
  pastProjects: string[];
}
export { type Person };

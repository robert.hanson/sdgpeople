import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import taiwindcss from '@tailwindcss/vite';

// https://vitejs.dev/config/
export default defineConfig({
build: {
    target: 'esnext',
    outDir: './public'
  },
  publicDir: 'staticAssets',
  //base: '/sdgpeople/',
  plugins: [
    vue(),
    taiwindcss()
  ],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  }
})

export async function msGraphGet(endpoint: string, accessToken: string, decodeMethod: string) {
  const headers = new Headers();
  const bearer = `Bearer ${accessToken}`;

  headers.append('Authorization', bearer);

  const options = {
    method: 'GET',
    headers: headers
  };

  return fetch(endpoint, options)
    .then(async (response) => {
      if (decodeMethod === 'json') {
        return response.json();
      }

      if (decodeMethod === 'image') {
        if (response.status !== 200) {
          if (response.status !== 404) {
            return null;
          }
          return '';
        }
        const url = window.URL || window.webkitURL;
        const blob = await response.blob();
        const blobUrl = url.createObjectURL(blob);
        return blobUrl;
      }

      return response.blob();
    })
    .catch((error) => {
      console.log('ERROR', error);
      return null;
    });
}

interface AdminList {
  name: string;
  id: string;
}

export type { AdminList };

async function hashBlob(blob: Blob, algorithm = 'SHA-256') {
  const arrayBuffer = await blob.arrayBuffer();
  const hashBuffer = await crypto.subtle.digest(algorithm, arrayBuffer);
  const hashArray = Array.from(new Uint8Array(hashBuffer));
  const hashHex = hashArray.map((b) => b.toString(16).padStart(2, '0')).join('');
  return hashHex;
}

async function hashBlobUrl(blobUrl: string) {
  const response = await fetch(blobUrl);
  const blob = await response.blob();
  const hash = await hashBlob(blob);
  return hash;
}
export default hashBlobUrl;

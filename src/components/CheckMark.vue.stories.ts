import type { Meta, StoryObj } from '@storybook/vue3';
import '../assets/main.css';
import CheckMark from './CheckMark.vue';

const meta = {
  component: CheckMark,
  tags: ['autodocs'],
  args: {},
  render: (args: any) => ({
    components: { CheckMark },
    setup() {
      return { args };
    },
    template: '<CheckMark v-bind="args"></CheckMark>'
  })
} satisfies Meta<typeof CheckMark>;

export default meta;
type Story = StoryObj<typeof meta>;

export const Playground: Story = {
  args: { ...meta.args }
};

export const CustomDelayAndColor: Story = {
  args: {}
};
